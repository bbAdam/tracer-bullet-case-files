// CREATIVE - TCW UPGRADE
// AUTHOR: ADAM CRUSE - SENIOR WEB DEVELOPER - BLACKBOARD, INC.
// VERSION: 10.27.21


$(function() {
    CreativeTCWUpgrade.Init();
});

var CreativeTCWUpgrade = {
	"Init": function(){

		var initialize = false;

		//CHECK TO MAKE SURE THE TEMPLATE IS IFRAMED, OTHERWISE DONT DO ANYTHING
		if(window.self !== window.top){ //TEMPLATE IS IFRAMED
			var parentURL = document.referrer;
			var iFrameURL = window.location.protocol+"//"+window.location.host+"/";

			var trimmedParentURL = parentURL.split("/");
			trimmedParentURL = trimmedParentURL[2];

			var trimmedIframeURL = iFrameURL.split("/");
			trimmedIframeURL = trimmedIframeURL[2];

			//CHECK TO MAKE SURE THE TEMPLATE ISNT IFRAMED INTO A SITE WITH A DIFFERENT DOMAIN
			//OTHERWISE IT WOULD THROW A CROSS DOMAIN ERROR (ALLY, etc.)
			if(trimmedParentURL == trimmedIframeURL){
				initialize = true;
			}
		}

		if(initialize){ //THE TEMPLATE IS IFRAMED AND ON A SITE WITH THE SAME DOMAIN AS THE PARENT WINDOW
			// CHECK TO MAKE SURE THE TEMPLATE IS IN THE TCW BEFORE DOING ANYTHING ELSE
			if($('body', window.parent.document).hasClass("sw-ide")){
				var _this = this;

				if(!$("#accordion", window.parent.document).hasClass("tcw-upgraded")){
					this.ApplyStyles();
					this.BuildFlyOut();
					this.FilesAndFoldersButton();
					this.ImageNames();
				}

				this.AppPreview();
				this.MoveApps();
				this.AppPreviewToggle();
				this.AccordionActions();
				this.RemoveTCWUpgrade();
				this.RemoveAppPreview();
				this.DragAndDrop();

				//FOR TEMPLATE SWITCHING
				setTimeout(function(){
					if(!$('#templateconfiguration-pnl-configure #cs-tcw-upgrade', window.parent.document).length){
						_this.BuildFlyOut();
					}
				}, 5000)

				$(window).resize(function() { _this.WindowResize(); });
			}
		}

	},

    "WindowResize": function() {
        this.AccordionReset();
    },

    "AccordionReset": function(){
    	//THERE IS A FUNCTION IN THE PRODUCT THAT RESETS THE VISIBLE ELEMENT SET TO THE FIRST ONE WHEN THE SCREEN IS RESIZED,
    	//TO ACCOMODATE THIS, WE NEED TO RESET THE ACTIVE ELEMENT SET HEADER
    	$('div#cs-tcw-upgrade .cs-element-set-header', window.parent.document).attr("aria-selected","false").removeClass("active");
    	$('div#cs-tcw-upgrade .cs-element-set-header:first-child', window.parent.document).attr("aria-selected","true").addClass("active");
    },

	"ApplyStyles": function(){
		//INJECT STYLES FOR THE FLYOUT MENU INTO THE PARENT DOCUMENT
		$('head', window.parent.document).append('<style type="text/css" id="tcw-upgrade-css">'+
		'@font-face {'+
			'font-family: "tcw-upgrade";'+
			'src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBVUAAAC8AAAAYGNtYXAXVtKJAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5ZvmlY34AAAF4AAABPGhlYWQXXb7yAAACtAAAADZoaGVhCCsEKAAAAuwAAAAkaG10eBCkAAAAAAMQAAAAHGxvY2EAngEMAAADLAAAABBtYXhwAAoAKwAAAzwAAAAgbmFtZa8Uj4wAAANcAAABknBvc3QAAwAAAAAE8AAAACAAAwMpAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpAgPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6QL//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAD/wAJAA8AAKAAABTgBMSImJwEuATU0NjcBPgEzMhYVFAYHMQkBHgEVFAYHMQ4BIzgBOQECAA4XCP5ACQoKCQHACBgNGyUKCf5tAZMJCgoJCBcOQAoJAcAIGA0NGAgBwAkKJRsNGAj+bf5tCBgNDRgICQoAAAACAAD/vwQAA8AABAAJAAATNwEHAREBFwEnAEQDvEP8QwO9Q/xERAN8RPxERAO8/IcDvEP8Q0QAAAIAAP/ABGgDwAAGAA0AAAkBJwkBNwEFAScJATcBAl3+aMUBRP68xQGcAgP+Y8QBRP7AxAGdAbH+D3ABiQGbbP4PHv4PcAGJAZts/g8AAAAAAQAAAAEAAD5A+qVfDzz1AAsEAAAAAADZ6z07AAAAANnrPTsAAP+/BGgDwAAAAAgAAgAAAAAAAAABAAADwP/AAAAEZAAA//wEaAABAAAAAAAAAAAAAAAAAAAABwQAAAAAAAAAAAAAAAIAAAACQAAABAAAAARkAAAAAAAAAAoAFAAeAFoAdgCeAAEAAAAHACkAAgAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQAIAAAAAQAAAAAAAgAHAGkAAQAAAAAAAwAIADkAAQAAAAAABAAIAH4AAQAAAAAABQALABgAAQAAAAAABgAIAFEAAQAAAAAACgAaAJYAAwABBAkAAQAQAAgAAwABBAkAAgAOAHAAAwABBAkAAwAQAEEAAwABBAkABAAQAIYAAwABBAkABQAWACMAAwABBAkABgAQAFkAAwABBAkACgA0ALBkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRWZXJzaW9uIDEuMABWAGUAcgBzAGkAbwBuACAAMQAuADBkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRSZWd1bGFyAFIAZQBnAHUAbABhAHJkaXN0cmljdABkAGkAcwB0AHIAaQBjAHRGb250IGdlbmVyYXRlZCBieSBJY29Nb29uLgBGAG8AbgB0ACAAZwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAC4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") format("truetype");'+
			'font-weight: normal;'+
			'font-style: normal;'+
			'font-display: block;'+
		'}'+
		'.tcw-upgrade {'+
			'font-family: "tcw-upgrade" !important;'+
			'speak: none;'+
			'font-style: normal;'+
			'font-weight: normal;'+
			'font-variant: normal;'+
			'text-transform: none;'+
			'line-height: 1;'+
			'-webkit-font-smoothing: antialiased;'+
			'-moz-osx-font-smoothing: grayscale;'+
		'}'+
		'.ui-accordion-header {'+
			'display:none;'+
		'}'+
		'#cs-tcw-upgrade {'+
			'position:relative;'+
		'}'+
		'#cs-element-flyout {'+
			'position:absolute;'+
			'left:100%;'+
			'top:0px;'+
			'width:220px;'+
			'overflow:hidden;'+
			'-ms-transition: all .3s ease 0s;'+
			'-moz-transition: all .3s ease 0s;'+
			'-webkit-transition: all .3s ease 0s;'+
			'transition: all .3s ease 0s;'+
		'}'+
		'#cs-element-flyout-inner {'+
			'background: #262626;'+
			'border:1px solid #767676;'+
		'}'+
		'#cs-element-flyout.closed {'+
			'width:0px;'+
		'}'+
		'#templateconfiguration-pnl-configure #accordion {'+
		    'background:transparent;'+
		    'display: flex;'+
			'display: -webkit-box;'+
			'display: -ms-flexbox;'+
			'display: -webkit-flex;'+
		    'flex-direction: column;'+
		    '-webkit-flex-direction: column;'+
		    'border:0px;'+
		    'border-radius:0px;'+
		    'height: calc(100vh - 261px);'+
		    'justify-content: flex-end;'+
		    '-webkit-justify-content: flex-end;'+
		    'width:auto;'+
		'}'+
		'#templateconfiguration-pnl-configure #accordion.ui-accordion .cs-accordion-content-active {'+
		    'height: auto !important;'+
		    'flex-grow:1;'+
		    '-webkit-flex-grow: 1;'+
		    'margin:0;'+
		    'padding:10px 22px 100px !important;'+
		    'border:0;'+
		'}'+
		'#templateconfiguration-pnl-configure #accordion.ui-accordion .cs-accordion-content-active::after {'+
		    'height: 80px;'+
		    'display:block;'+
		    'content: "";'+
		'}'+
		'#cs-tcw-element-set-list {'+
		    'padding: 0px;'+
		    'margin: 0px !important;'+
		    'max-height: calc(100vh - 330px);'+
		    'overflow: auto;'+
		'}'+
		'#cs-element-flyout-toggle {'+
		    'color: #ffffff;'+
		    'font-size:14px;'+
		    'padding: 8px 15px;'+
		    'background: #404040;'+
		    'opacity: 0;'+
		    '-ms-transition: all .3s ease 0s;'+
			'-moz-transition: all .3s ease 0s;'+
			'-webkit-transition: all .3s ease 0s;'+
			'transition: all .3s ease 0s;'+
			'position:relative;'+
		'}'+
		'#cs-element-flyout-toggle span {'+
			'position:absolute;'+
			'right:10px;'+
			'top:10px;'+
			'font-size:11px;'+
			'cursor:pointer;'+
		'}'+
		'#cs-element-flyout-toggle.closed {'+
		    'opacity: 1;'+
		    'cursor:pointer;'+
		'}'+
		'#cs-tcw-element-set-list-header {'+
			'font-size:16px;'+
			'font-weight:600;'+
			'font-family:"OpenSans-SemiBold", sans-serif;'+
			'color:#fff;'+
			'padding:12px 30px 12px 15px;'+
			'position:relative;'+
		'}'+
		'#cs-tcw-element-set-list-header span {'+
			'position:absolute;'+
			'right:10px;'+
			'top:12px;'+
			'cursor:pointer;'+
		'}'+
		'.cs-element-set-header {'+
		    'color: #ffffff;'+
		    'font-size:14px;'+
		    'padding: 12px 30px;'+
		    'line-height: 1.2;'+
		    'border-bottom: 1px solid #000;'+
		    'position:relative;'+
		    '-ms-transition: all .3s ease 0s;'+
			'-moz-transition: all .3s ease 0s;'+
			'-webkit-transition: all .3s ease 0s;'+
			'transition: all .3s ease 0s;'+
		'}'+
		'.cs-element-set-header:last-child {'+
		    'border-bottom: 0px;'+
		'}'+
		'.cs-element-set-header.active {'+
			'background: #fff;'+
			'color:#262626;'+
		'}'+
		'.cs-element-set-header span {'+
			'position:absolute;'+
			'left:11px;'+
			'top:calc(50% - 7px);'+
			'opacity:0;'+
			'-ms-transition: all .3s ease 0s;'+
			'-moz-transition: all .3s ease 0s;'+
			'-webkit-transition: all .3s ease 0s;'+
			'transition: all .3s ease 0s;'+
		'}'+
		'.cs-element-set-header.active span {'+
			'opacity:1;'+
		'}'+
		'.ui-accordion-content-active {'+
			'display:none !important;'+
		'}'+
		'.cs-accordion-content-active {'+
			'display:block !important;'+
		'}'+
		'#templateconfiguration-btn-save {'+
			'margin-left:10px !important;'+
		'}'+
		'#cs-app-preview {'+
			'position:relative;'+
			'background:#a234b5;'+
			'order:2;'+
			'padding-right:30px;'+
		'}'+
		'#cs-app-preview::after {'+
			'width: 0;'+
			'height: 0;'+
			'border-left: 5px solid transparent;'+
			'border-right: 5px solid transparent;'+
			'border-top: 5px solid #fff;'+
			'display:block;'+
			'content:"";'+
			'position:absolute;'+
			'right:12px;'+
			'top:17px;'+
		'}'+
		'#app-preview-dd {'+
			'position:absolute;'+
			'top:calc(100% + 15px);'+
			'right:10px;'+
			'background:#262626;'+
			'color:#000;'+
			'min-height:100px;'+
			'padding:10px;'+
			'width:447px;'+
			'white-space:break-spaces;'+
			'display:none;'+
			'border-radius:2px;'+
		'}'+
		'#app-preview-dd::before {'+
			'width: 0;'+
			'height: 0;'+
			'border-left: 8px solid transparent;'+
			'border-right: 8px solid transparent;'+
			'border-bottom: 9px solid #262626;'+
			'display:block;'+
			'content:"";'+
			'position:absolute;'+
			'right:150px;'+
			'bottom:100%;'+
		'}'+
		'#cs-app-preview-column-container {'+
			'display: flex;'+
			'display: -webkit-box;'+
			'display: -ms-flexbox;'+
			'display: -webkit-flex;'+
		'}'+
		'.app-preview-column.one {'+
			'max-width: 150px;'+
		    'font-size: 11px;'+
		    'color: #fff;'+
		    'display: flex;'+
			'display: -webkit-box;'+
			'display: -ms-flexbox;'+
			'display: -webkit-flex;'+
		    'flex-direction: column;'+
		    'align-items: flex-start;'+
		    '-webkit-flex-direction: column;'+
		    '-webkit-align-items: flex-start;'+
		    'position: relative;'+
		    'border-right: 1px solid #000;'+
		    'box-sizing: border-box;'+
		    'padding-right: 10px;'+
		'}'+
		'.app-preview-column.two {'+
			'flex-grow:1;'+
		    '-webkit-flex-grow:1;'+
		'}'+
		'.cs-available-region {'+
			'border:1px solid #000;'+
			'padding:10px;'+
		'}'+
		'#cs-app-preview:hover {'+
			'opacity:1 !important;'+
		'}'+
		'.cs-app-preview-row.current {'+
			'padding-bottom:10px;'+
			'max-height:400px;'+
			'overflow:auto;'+
		'}'+
		'.cs-app-preview-row.button {'+
			'padding:10px 0px 10px 10px;'+
			'display: flex;'+
			'display: -webkit-box;'+
			'display: -ms-flexbox;'+
			'display: -webkit-flex;'+
			'justify-content:space-between;'+
			'-webkit-justify-content:space-between;'+
		'}'+
		'.cs-app-region-row {'+
			'padding:8px 0px 8px 10px;'+
		'}'+
		'.cs-app-data {'+
			'display:block;'+
			'min-height:30px;'+
			'box-sizing:border-box;'+
			'border:1px solid #cdcdcd;'+
			'border-radius:2px;'+
			'padding:10px;'+
			'margin:2px 0px !important;'+
			'background:#fff;'+
		'}'+
		'.cs-empty-li {'+
			'min-height:5px;'+
		'}'+
		'.cs-region-heading {'+
			'padding-bottom:5px;'+
			'margin-bottom:5px;'+
			'color:#fff;'+
			'font-weight:700;'+
		'}'+
		'.cs-app-preview-button {'+
			'background:#777;'+
			'color:#fff;'+
			'padding:9px 13px;'+
			'border:0;'+
			'cursor:pointer;'+
			'-ms-transition: all .4s ease 0s;'+
			'-moz-transition: all .4s ease 0s;'+
			'-webkit-transition: all .4s ease 0s;'+
			'transition: all .4s ease 0s;'+
		'}'+
		'.cs-app-preview-button:hover {'+
			'background:#5d5c5c;'+
		'}'+
		'#cs-app-preview-instructions {'+
			'list-style-type: decimal;'+
    		'padding-left: 11px;'+
    		'margin-top: 10px;'+
		'}'+
		'#cs-app-preview-instructions li {'+
    		'padding: 5px 0px;'+
    		'line-height: 1.3;'+
		'}'+
		'#cs-disclaimer {'+
    		'position: absolute;'+
    		'bottom: 0px;'+
    		'left: 5px;'+
    		'width: calc(100% - 5px);'+
    		'max-height: none !important;'+
		'}'+
        '#ImagePickerDialog {'+
    		'margin-bottom: 0;'+
    		'height: calc(100% - 14%);'+
    		'top: 5% !important;'+
		'}'+
        '#dialog-overlay-ImagePickerDialog-body {'+
    		'height: 100%;'+
    		'box-sizing: border-box;'+
		'}'+
        '#dialog-overlay-ImagePickerDialog-body div.ui-widget.dialog {'+
    		'height: 100%;'+
            'display: flex;'+
			'display: -webkit-box;'+
			'display: -ms-flexbox;'+
			'display: -webkit-flex;'+
            'flex-direction: column;'+
            '-webkit-flex-direction: column;'+
		'}'+
        'div.ui-dialog-overlay-base-modal #dialog-overlay-ImagePickerDialog-body div.ui-widget-detail.scroll-300 {'+
    		'flex-grow: 1;'+
            '-webkit-flex-grow: 1;'+
			'max-height: none;'+
			'height: auto;'+
		'}'+
		'</style>');
	},

	"DragAndDrop": function(){
		if(!$("#tcw-upgrade-js", window.parent.document).length){
			//INJECT SCRIPT FOR APP RE-ARRANGEMENT IN APP PREVIEW
			var script = window.parent.document.createElement('script');
			script.id = 'tcw-upgrade-js';
			script.text = 'let selected = null\n'+

				'function dragOver(e) {\n'+
				  'if (isBefore(selected, e.target)) {\n'+
				    'e.target.parentNode.insertBefore(selected, e.target)\n'+
				 '} else {\n'+
				    'e.target.parentNode.insertBefore(selected, e.target.nextSibling)\n'+
				  '}\n'+
				'}\n'+

				'function dragEnd() {\n'+
				  'selected = null\n'+
				  'upDateAppRegion();\n'+
				'}\n'+

				'function dragStart(e) {\n'+
				  'e.dataTransfer.effectAllowed = "move"\n'+
				  'e.dataTransfer.setData("text/plain", null)\n'+
				  'selected = e.target\n'+
				'}\n'+

				'function isBefore(el1, el2) {\n'+
				  'let cur\n'+
				  'if (el2.parentNode === el1.parentNode) {\n'+
				    'for (cur = el1.previousSibling; cur; cur = cur.previousSibling) {\n'+
				      'if (cur === el2) return true\n'+
				    '}\n'+
				  '}\n'+
				  'return false;\n'+
				'}\n'+

				'function upDateAppRegion(){\n'+
					'$(".cs-app-region-row").each(function(){\n'+
						'var currentRegion = $(this).find(".cs-region-heading").attr("data-region-id");\n'+
						'$(this).find(".cs-app-data").attr("data-current-region",currentRegion);\n'+
					'})\n'+
				'}';
			window.parent.document.getElementsByTagName('head')[0].appendChild(script);
		}
	},

	"BuildFlyOut": function(){
		var elementSets = '';

		//LOOP THROUGH ALL ELEMENT SETS AND GRAB THEIR INFORMATION TO BUILD THE FLYOUT
		$('div#accordion .ui-accordion-header', window.parent.document).each(function(){
			var accordionHeaderID = $(this).attr("id");
			var accordionHeaderText = $(this).text();

			//EACH ELEMENT SET HEADER AND PANEL HAS A UNIQUE ID NUMBER, WE NEED TO GRAB THAT
			var rawaccordionPanelNumber = accordionHeaderID.split("-");
			//CHECK FOR JQUERY UI UPGRADE IN THE PRODUCT
			if(rawaccordionPanelNumber[4] == undefined){ //JQUERY UI 1.12.0
				var accordionPanelNumber = parseInt(rawaccordionPanelNumber[2])+1;
				//JQUERY UI LI ID STRUCTURE = ui-id-0

				//BUILD HTML FOR ELEMENT SET HEADER
				elementSets += '<li class="cs-element-set-header" aria-controls="ui-id-'+accordionPanelNumber+'" data-accordion-panel="ui-id-'+accordionPanelNumber+'"><span class="tcw-upgrade" aria-hidden="true">&#xe900;</span>'+accordionHeaderText+'</li>';
			} else { //JQUERY UI 1.10.1
				var accordionPanelNumber = rawaccordionPanelNumber[4];
				//JQUERY UI LI ID STRUCTURE = ui-accordion-accordion-header-0

				//BUILD HTML FOR ELEMENT SET HEADER
				elementSets += '<li class="cs-element-set-header" aria-controls="ui-accordion-accordion-panel-'+accordionPanelNumber+'" data-accordion-panel="ui-accordion-accordion-panel-'+accordionPanelNumber+'"><span class="tcw-upgrade" aria-hidden="true">&#xe900;</span>'+accordionHeaderText+'</li>';
			}


		})

		//BUILD HTML FOR FLYOUT
		var elementsFlyOut = 	'<div id="cs-tcw-upgrade">'+
									'<div id="cs-element-flyout-toggle" aria-controls="cs-element-flyout" aria-expanded="true">Template Options<span class="tcw-upgrade">&#xe902;</span></div>'+
									'<div id="cs-element-flyout" aria-hidden="false">'+
										'<div id="cs-element-flyout-inner">'+
											'<div id="cs-tcw-element-set-list-header">Template Options<span class="tcw-upgrade">&#xe901;</span></div>'+
											'<ul id="cs-tcw-element-set-list">'+elementSets+'</ul>'+
										'</div>'+
									'</div>'+
								'</div>';

		//ADD FLYOUT TO THE PARENT DOM
		$('#templateconfiguration-pnl-configure', window.parent.document).prepend(elementsFlyOut);
		//SET THE FIRST ELEMENT SET TO ACTIVE
		$('div#cs-tcw-upgrade .cs-element-set-header:first-child', window.parent.document).attr("aria-selected","true").addClass("active");
		$('div#accordion .ui-accordion-content:nth-child(2)', window.parent.document).addClass("cs-accordion-content-active");
		//ADD CLASS TO ACCORDION TO KEEP FUNCTIONS FROM RUNNING EACH TIME THE TEMPLATE IS SAVED
		$("#accordion", window.parent.document).addClass("tcw-upgraded");
		this.AccordionActions();
	},

	"AccordionActions": function(){
		//OPEN ACTIVE PANEL AFTER SAVE AND PREVIEW
		var panelOnRefresh = $('div#cs-tcw-upgrade .cs-element-set-header.active', window.parent.document).attr("data-accordion-panel");
		$('div#accordion .ui-accordion-content', window.parent.document).removeClass("cs-accordion-content-active").attr("aria-expanded","false");
		$('div#accordion .ui-accordion-content#'+panelOnRefresh, window.parent.document).addClass("cs-accordion-content-active").show().attr("aria-expanded","true");

		//WHEN AN ELEMENT SET HEADER IS CLICKED, WE NEED TO HIDE THE CURRENT ELEMENT PANEL AND SHOW THE PANEL THAT CORRESPONDS TO THE CLICKED HEADER
		$('div#cs-tcw-upgrade .cs-element-set-header', window.parent.document).click(function(){
			var panelToOpen = $(this).attr("data-accordion-panel");
			var panelText = $(this).text();

			$('div#cs-tcw-upgrade .cs-element-set-header', window.parent.document).attr("aria-selected","false").removeClass("active");
			$(this).attr("aria-selected","true").addClass("active");
			$('div#accordion .ui-accordion-content', window.parent.document).removeClass("cs-accordion-content-active").attr("aria-expanded","false");
			$('div#accordion .ui-accordion-content#'+panelToOpen, window.parent.document).addClass("cs-accordion-content-active").show().attr("aria-expanded","true");
		})

		//CLOSE FLYOUT
		$('#cs-tcw-element-set-list-header span', window.parent.document).click(function(){
			$('#cs-element-flyout', window.parent.document).addClass("closed");
			$("#cs-element-flyout-toggle", window.parent.document).addClass("closed");
		})

		//OPEN FLYOUT (IS INITALLY OPENED BY DEFAULT)
		$('#cs-element-flyout-toggle', window.parent.document).click(function(){
			$(this).removeClass("closed");
			$('#cs-element-flyout', window.parent.document).removeClass("closed");
		})
	},

	"ImageNames": function(){
		//THIS FUNCTION ADDS A TITLE ATTRIBUTE TO IMAGES WITHIN THE IMAGE PICKER
		//INITIAL LOADED IMAGES
		$('body', window.parent.document).on("click", ".image-picker-current-image", function(){
			setTimeout(function(){
				$(".image-picker-image-list li", window.parent.document).each(function(){
					var fileName = $(this).attr("file").split("/");
					$(this).attr("title", fileName[1]);
				})
			}, 2000)

		})

		//EACH TIME A FOLDER IS SELECTED
		$('body', window.parent.document).on("click", ".image-picker-category-list", function(){
			setTimeout(function(){
				$(".image-picker-image-list li", window.parent.document).each(function(){
					var fileName = $(this).attr("file").split("/");
					$(this).attr("title", fileName[1]);
				})
			}, 1000)
		})
	},

	"RemoveTCWUpgrade": function(){
		//REMOVES STYLES FROM PARENT WINDOW WHEN TEMPLATES ARE CHANGED.
		//WITHOUT THIS FUNCTION, IF A TEMPLATE IS SELECTED THAT DOES NOT USE THE TCW UPGRADE, NO ELEMENTS WILL SHOW IN THE LEFT PANEL
		$('body', window.parent.document).on("click", "ul#templatepicker-lst-templatelistList li", function(){
			$("#tcw-upgrade-css, #cs-tcw-upgrade, #cs-app-preview, #app-preview-dd",window.parent.document).remove();
			$("#accordion", window.parent.document).removeClass("tcw-upgraded");

			//REMOVE APP PREVIEW BORDER FROM IFRAME IF TEMPLATE CHANGES
			$('#templateconfiguration-iframe-preview', window.parent.document).css("border","0px");
		})
	},

	"RemoveAppPreview": function(){
		//REMOVE APP PREVIEW BORDER FROM IFRAME IF TEMPLATE CHANGES
		$('body', window.parent.document).on("click", "#templateconfiguration-btn-preview", function(){
			$('#templateconfiguration-iframe-preview', window.parent.document).css("border","0px");

		})
	},

	"FilesAndFoldersButton": function(){
		if(!$('#cs-upload-images', window.parent.document).length){
			$('#templateconfiguration-btn-preview', window.parent.document).after('<a class="ui-btn-toolbar ui-btn-toolbar-primary" id="cs-upload-images" target="popup" onclick="window.open(\'/cms/UserControls/FilesAndFolders/FilesAndFolders.aspx?FolderType=TemplateLibrary\',\'popup\'); return false;" title="Upload images to Use in this template."><span>Upload Images</span></a>');
		}

	},

	"AppPreview": function(){
		//BUILD APP PREVIEW BUTTON AND DROPDOWN IN TCW HEADER
		if(!$('#cs-app-preview', window.parent.document).length){
			$('#templateconfiguration-btn-save', window.parent.document).after('<div class="ui-btn-toolbar ui-btn-toolbar-primary" id="cs-app-preview"><span>App Preview</span></div><div id="app-preview-dd"></div>');
		}

		//SETUP AN ARRAY FOR REGION IDs
		var regionIDs = [];

		//LOOP THROUGH APP REGIONS TO BUILD APP REGION ARRAY
		$("#gb-page .ui-hp").each(function(){
			//GET REGION ID
			var trueRegionNumber = $(this).attr("id");

			//PUSH REGION ID INTO ARRAY
			regionIDs.push(trueRegionNumber);
		})

		//LOOP THROUGH APP REGIONS TO CREATE A LIST OF ALL APPS IN THAT REGION
		var counter = 0;
		var currentAppPositions = '';
		$("#gb-page .ui-hp").each(function(){

			switch(counter){
				case 0:
					var currentRegion = "A";
				break;
				case 1:
					var currentRegion = "B";
				break;
				case 2:
					var currentRegion = "C";
				break;
				case 3:
					var currentRegion = "D";
				break;
				case 4:
					var currentRegion = "E";
				break;
				case 5:
					var currentRegion = "F";
				break;
				case 6:
					var currentRegion = "G";
				break;
				case 7:
					var currentRegion = "H";
				break;
				case 8:
					var currentRegion = "I";
				break;
				case 9:
					var currentRegion = "J";
				break;
				case 10:
					var currentRegion = "K";
				break;
				case 11:
					var currentRegion = "L";
				break;
			}

			var regionAppNames = '';

			//LOOP THROUGH ALL APPS WITHIN THIS REGION TO BUILD A LISTING
			$(this).find(".app").each(function(){
				var appTitle = $.trim($(this).find(".ui-widget-header h1").text());

				if(appTitle != ""){ //THE APP HAS A TITLE, GRAB IT TO PLACE AFTER THE APP TYPE
					appTitle = " ("+appTitle+")";
				}

				//USE THE APP CLASS TO DETERMINE WHICH TYPE OF APP IT IS
				var cleanAppClasses = $(this).attr("class");
				var appClass = cleanAppClasses.split(" ");
				appClass = appClass.slice(-1)[0];

				if(appClass == "last-app" || appClass == "detail" || appClass == "app"){ //THE LAST CLASS ON THE APP DOESNT TELL US WHAT THE APP IS, GRAB THE SECOND-TO-LAST CLASS
					appClass = cleanAppClasses.split(" ");
				appClass = appClass.slice(-2, -1)[0];
				}

				//GET CLEAN APP NAMES FROM APP CLASS
				var cleanAppName = "";
				switch(appClass){
					case "multimedia-gallery":
						cleanAppName = "MMG";
					break;
					case "cs-rs-multimedia-rotator":
						cleanAppName = "MMR";
					break;
					case "upcomingevents":
						cleanAppName = "Upcoming Events";
					break;
					case "flexpage":
						cleanAppName = "Content";
					break;
					case "blog":
						cleanAppName = "Blog";
					break;
					case "headlines":
						cleanAppName = "Headlines";
					break;
					case "facts-and-figures":
						cleanAppName = "Facts & Figures";
					break;
					case "announcements":
						cleanAppName = "Announcements";
					break;
					case "bell-schedule":
						cleanAppName = "Bell Schedule";
					break;
					case "assignment":
						cleanAppName = "Assignment";
					break;
					case "article-library":
						cleanAppName = "Article Library";
					break;
					case "google-events-calendar":
						cleanAppName = "Google Events Calendar";
					break;
					case "image":
						cleanAppName = "Image";
					break;
					case "content-accordion":
						cleanAppName = "Content Accordion";
					break;
					case "school-directory":
						cleanAppName = "School Directory";
					break;
					case "siteshortcuts":
						cleanAppName = "Site Shortcuts";
					break;
					case "lunchmenu":
						cleanAppName = "Lunch Menu";
					break;
					case "link-library":
						cleanAppName = "Link Library";
					break;
					case "icons-horizontal":
						cleanAppName = "Icons Horizontal";
					break;
					case "heading":
						cleanAppName = "Heading";
					break;
					case "alumni-directory":
						cleanAppName = "Alumni Directory";
					break;
					case "board-policies":
						cleanAppName = "Board Policies";
					break;
					case "book-list":
						cleanAppName = "Book List";
					break;
					case "maps-directions":
						cleanAppName = "Maps & Directions";
					break;
					case "divider-app":
						cleanAppName = "Divider";
					break;
					case "file-library":
						cleanAppName = "File Library";
					break;
					case "idea-board":
						cleanAppName = "Idea Board";
					break;
					case "twitter-retweet":
						cleanAppName = "Twitter Retweet";
					break;
					case "about-teacher":
						cleanAppName = "About Teacher";
					break;
					case "table":
						cleanAppName = "Table";
					break;
					case "tabbed-content":
						cleanAppName = "Tabbed Content";
					break;
					case "staffdirectorydiv":
						cleanAppName = "Staff Directory";
					break;
					case "program-portal":
						cleanAppName = "Program Portal";
					break;
					case "podcast":
						cleanAppName = "Podcast";
					break;
					case "figure":
						cleanAppName = "Figure";
					break;
					case "facebook-like":
						cleanAppName = "Facebook Like";
					break;
					case "document-viewer":
						cleanAppName = "Document Viewer";
					break;
					case "discussion":
						cleanAppName = "Discussion";
					break;
					case "content-accordion":
						cleanAppName = "Content Accordion";
					break;
					case "calendar":
						cleanAppName = "Calendar";
					break;
					case "heading":
						cleanAppName = "Heading";
					break;
					case "heading":
						cleanAppName = "Heading";
					break;
					case "heading":
						cleanAppName = "Heading";
					break;
					default:
						cleanAppName = "App";
					break;
				}

				//GET THE PMI FROM THE APP (THIS IS WHAT WE MOVE WHEN WE MOVE APPS)
				var appPMI = $(this).closest("[id*='pmi']").attr("id");

				//CREATE THE HTML FOR THE APP LISTING
				regionAppNames += "<li class='cs-app-data' draggable='true' ondragend='dragEnd()' ondragover='dragOver(event)' ondragstart='dragStart(event)' data-app-pmi='"+appPMI+"' title='"+appTitle+"'>"+cleanAppName+"</li>";
			})

			//CREATE THE ROW THAT WILL HOLD ALL DATA FOR THIS REGION
			currentAppPositions += "<ul class='cs-app-region-row'><li class='cs-region-heading' data-region-id='"+regionIDs[counter]+"'>Region "+currentRegion+ ":</li>"+regionAppNames+"<li class='cs-empty-li' draggable='true' ondragend='dragEnd()' ondragover='dragOver(event)' ondragstart='dragStart(event)'></li></ul>";

			counter++;
		})

		//UPDATE THE HTML IN THE APP PREVIEW DROPDOWN TO REFLECT CURRENT APP POSITIONS WITHIN THE TEMPLATE
		$('#app-preview-dd', window.parent.document).html("<div id='cs-app-preview-column-container'><div class='app-preview-column one'><p><strong style='font-size:12px;'>App Preview</strong></p><ol id='cs-app-preview-instructions'><li>Drag and drop apps into desired regions.*</li><li>Click 'Preview Apps in New Regions' to display apps in new regions.</li><li>Clicking 'Save & Preview' will reset the apps to their locations on your live website.</li></ol><p id='cs-disclaimer'>*App Preview does NOT change app placement on your live website. To do that, use the Homepage Editor in the Site Workspace.</p></div><div class='app-preview-column two'><div class='cs-app-preview-row current'>"+currentAppPositions+"</div><div class='cs-app-preview-row button'><button id='cs-app-preview-initiate' class='cs-app-preview-button'>Preview Apps in New Regions</button><button id='cs-app-preview-close-dd' class='cs-app-preview-button'>Close</button></div></div></div>");

		//LOOP THROUGH EACH REGION ROW AND CHANGE THE VALUES OF THE DATA-CURRENT-REGION ATTRIBUTE TO REFLECT THE REGION THEY ARE IN
		$('.cs-app-region-row', window.parent.document).each(function(){
			var initialSelection = $(this).find(".cs-region-heading").attr("data-region-id");
			$(this).find(".cs-app-data").attr("data-current-region",initialSelection);
		})

		//GET THE IFRAME SRC OF THE PREVIEW WINDOW TO ADD APP AND REGION URL PARAMETERS TO
		var iFrameSrc = $("#templateconfiguration-iframe-preview", window.parent.document).attr("src");

		//INITIATE THE MOVING OF APPS TO THEIR NEW PREVIEW POSITIONS
		$('#cs-app-preview-initiate', window.parent.document).on("click", function(){
			//SETUP VARIABLE FOR APP AND REGION URL PARAMETERS
			var appRegionUrlParameters = '&csAppPreviewData=';

			//LOOP THROUGH EACH APP IN THE APP PREVIEW DROPDOWN AND GRAB THEIR SELECT VALUES TO ADD TO URL PARAMETERS
			$(".cs-app-data", window.parent.document).each(function(){
				appRegionUrlParameters += $(this).attr("data-app-pmi")+'_'+$(this).attr("data-current-region")+'+';
			})

			//REMOVE THE CURRENT IFRAME PREVIEW AND REPLACE WITH NEW IFRAME THAT HAS APP AND REGION URL PARAMETERS
			$('#template-frame', window.parent.document).empty().html('<iframe id="templateconfiguration-iframe-preview" src="'+iFrameSrc+'&'+appRegionUrlParameters+'" frameborder="0" width="100%" scrolling="auto" style="height:calc(100vh - 200px); box-sizing:border-box; border:8px solid #a234b5;"></iframe>');
		});

	},

	"AppPreviewToggle": function(){
		//OPEN APP PREVIEW DROPDOWN
		$('#cs-app-preview', window.parent.document).click(function(){
			$('#app-preview-dd', window.parent.document).slideDown();
			$(this).addClass("open");
		});

		//CLOSE APP PREVIEW DROPDOWN
		$('#cs-app-preview-close-dd', window.parent.document).click(function(){
			$('#app-preview-dd', window.parent.document).slideUp();
			$('#cs-app-preview', window.parent.document).removeClass("open");
		});
	},

	"MoveApps": function(){
		//GET CURRENT URL
		var loadedUrl = window.location.href;

		var _this = this;

		if(loadedUrl.includes("csAppPreviewData")){ //THE CURRENT URL IS FROM THE TCW APP PREVIEW, MOVE THE APPS INTO THEIR NEW REGIONS
			//CLEAN UP URL PARAMETERS
			var appPositionParameters = loadedUrl.split("=");
			appPositionParameters = appPositionParameters.slice(-1)[0];
			//REMOVE TRAILING +
			appPositionParameters = appPositionParameters.slice(0,-1);

			//HIDE APP REGIONS
			$("#gb-page .ui-hp").hide();

			appPositionArray = appPositionParameters.split("+");

			var i;
			var pmiIDandRegion = "";
			var pmi = "";
			var appRegion = "";

			//LOOP THROUGH EACH APP REGION AND APP PARING IN THE URL PARAMETERS
			for (i = 0; i < appPositionArray.length; ++i) {
				pmiIDandRegion = appPositionArray[i].split("_");
				pmi = pmiIDandRegion[0];
				appRegion = pmiIDandRegion[1];

				//PLACE APP IN NEW PREVIEW REGION
				$("#"+pmi).appendTo("#"+appRegion);
			}

			//SHOW APP REGIONS
			$("#gb-page .ui-hp").show();

			//CLEAR APP PREVIEW DROPDOWN
			$('#app-preview-dd', window.parent.document).empty();

			//RERUN APP PREVIEW FUNCTION TO UPDATE APP PREVIEW DROPDOWN CONTENT TO MATCH NEW PREVIEW POSITIONS
			_this.AppPreview();
		}

	}
};
